/**
 * Returns rendered image bounds for object-fit=contain
 * Assumes the image fills the container
 */
const get_image_bounds = (img) => {
    const { x, y, width, height } = img.getBoundingClientRect();
      const iw = img.naturalWidth || img.width;
      const ih = img.naturalHeight || img.height;

      let rh = 0;
      let rw = 0;
      const iar = iw / ih;
      const ar = width / height;
      if (iar < ar) {
        // Screen space is wider than image
        rh = height;
        rw = rh * iar;
      } else {
        // Screen space is taller than image
        rw = width;
        rh = rw / iar;
      }

      return [img.offsetLeft + (width - rw) / 2, img.offsetTop + (height - rh) / 2, rw, rh];
}
const _is_object = (obj) => {
    // https://stackoverflow.com/a/14706877
    const type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
}
const create_random_id = (prefix) => {
    return `${prefix || "x"}-${Date.now()}-${(
      1e-5 +
      Math.random() * (1 - 1e-5)
    ).toString(36).substring(2, 10)}`;
};
const create_uuid = () => {
    const url = URL.createObjectURL(new Blob([]));
    const uuid = url.slice(-36);
    Promise.resolve().then(() => {
        URL.revokeObjectURL(url);
    });
    return uuid;
};
const TAU = 2 * Math.PI;

const path_utils = {
    options: {
        stroke: {
            color: 'green',
            lineWidth: '4',
        },
        fill: {
            color: 'rgba(0, 0, 0, 0.4)'
        },
        cp: {
            radius: 8,
        }
    },
    get_rect: ([x, y, w, h]) => {
        const p = new Path2D();
        p.rect(x, y, w, h);
        return p;
    },
    get_cp: (pts) => {
        const cp = new Path2D();
        const radius = path_utils.options.cp.radius;
        pts.forEach(([x, y]) => {
            cp.moveTo(x, y);
            cp.arc(x, y, radius, 0, TAU);
        });
        return cp;
    },
    rect_to_pts: (r) => {
        const [x, y, w, h] = r;
        return [
            [x, y],
            [x+w, y],
            [x+w, y+h],
            [x, y+h],
        ]
    }
}
const CURSOR_MAP = {
    't': 'ns-resize',
    'b': 'ns-resize',
    'r': 'ew-resize',
    'l': 'ew-resize',
    'tl': 'nwse-resize',
    'br': 'nwse-resize',
    'tr': 'nesw-resize',
    'bl': 'nesw-resize',
    'move': 'move',
    'disabled': 'not-allowed',
}
class DrawView {
    constructor(container) {
        this.c = container;
        this._updating = false;

        this.canvas = document.createElement('canvas');
        this.bcanvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.bctx = this.bcanvas.getContext('2d');

        this.hidden = true;
        this.c.appendChild(this.canvas);
    }

    get hidden() {
        return this._hidden;
    }
    set hidden(_val) {
        this._hidden = !!_val;
        if (this._hidden) {
            this.canvas.classList.add('gone');
        } else {
            this.canvas.classList.remove('gone');
        }
    }

    get width() {
        return this.canvas.width;
    }
    get height() {
        return this.canvas.height;
    }

    on_change() {
        const img = this.c.querySelector('img');
        if (!img) {
            this.hidden = true;
            return;
        }

        const [_x, _y, w, h] = get_image_bounds(img);

        this.canvas.width = this.bcanvas.width = w;
        this.canvas.height = this.bcanvas.height = h;

        this.canvas.style.left = `${_x}px`;
        this.canvas.style.top = `${_y}px`;
    }

    _update_pointer(loc) {
        this.canvas.style.cursor = CURSOR_MAP[loc] || 'default';
    }

    show() {
        if (this._updating) {
            return;
        }
        this._updating = true;
        window.requestAnimationFrame(() => {
            this.ctx.clearRect(0, 0, this.width, this.height);
            this.ctx.drawImage(this.bcanvas, 0, 0)
            this._updating = false;
        })
    }
    _relative_to_canvas(p){
        const { width, height } = this;
        const [x, y] = p
        return [
            x*width,
            y*height
        ]
    }
    _draw_path(p, options) {
        this.bctx.save();
        const { stroke, fill } = options;
        if (fill) {
            this.bctx.fillStyle = fill.color;
            this.bctx.fill(p);
        } else {
            this.bctx.strokeStyle = stroke.color;
            this.bctx.lineWidth = stroke.lineWidth;
            this.bctx.stroke(p);
        }
        this.bctx.restore();
    }
    _draw_path_and_cp([path, cp], fill) {
        this._draw_path(path, {
            ...(fill && { fill: path_utils.options.fill }),
            ...(!fill && { stroke: path_utils.options.stroke }),
        });
        if (cp) {
            this._draw_path(cp, {
                fill: {
                    color: 'red',
                }
            });
        }
    }
    get_path(shape, _params = null) {
        const params = {
            cp: true,
            absolute: false,
            ...(_is_object(_params) && _params)
        }
        let pts = shape;
        if (params.absolute) {
            pts = pts.reduce((_pts, xy, idx) => {
                if (idx % 2 !== 0) {
                    const _x = _pts.pop()
                    _pts.push([_x, xy]);
                } else {
                    _pts.push(xy);
                }
                return _pts;
            }, []).flatMap((xy) => {
                return [...this._relative_to_canvas(xy)]
            });
        }

        const path = path_utils.get_rect(pts);
        const cp = params.cp ? path_utils.get_cp(path_utils.rect_to_pts(pts)) : null;
        return [path, cp];
    }
    is_point_in_path([x, y], [path, cp], absolute) {
        if (absolute) {
            ([x, y] = this._relative_to_canvas([x, y]));
        }
        if (cp) {
            if (this.bctx.isPointInPath(cp, x, y)) {
                return 'cp';
            }
        }
        this.bctx.save();
        this.bctx.lineWidth = 2 * path_utils.options.cp.radius;
        const is_on_path = this.bctx.isPointInStroke(path, x, y);
        this.bctx.restore();
        if (is_on_path) {
            return 'on';
        }
        if (this.bctx.isPointInPath(path, x, y)) {
            return 'in';
        }
        return 'out';
    }
    draw(shapes, _params = null) {
        const params = {
            clear: true,
            fill: false,
            cp: true,
            absolute: false,
            show: true,
            ...(_is_object(_params) && _params)
        }
        if (params.clear) {
            this.bctx.clearRect(0, 0, this.width, this.height);
            delete params.clear
        }
        shapes.forEach((shape) => {
            const p = this.get_path(shape, { cp: params.cp, absolute: params.absolute });
            this._draw_path_and_cp(p, params.fill);
        });
        if (params.show) {
            this.show();
        }
    }
    _mouse_to_relative(e) {
        const { width, height } = this;
        const x = e.offsetX;
        const y = e.offsetY;
        return [ x / width, y / height];
    }
    bind_click_handler(id, handler){
        let offset = [-1, -1];
        let delta = [0, 0];
        let drag = false;
        let enabled = false;
        const _handler = (e) => {
            const [x, y] = this._mouse_to_relative(e);
            switch(e.type) {
                case 'mousedown':
                    offset = [x, y];
                    delta = [0, 0];
                    drag = false;
                    return handler(id, e.type, offset)
                case 'mousemove':
                    delta = [x - offset[0], y - offset[1]];
                    if (drag === false) {
                        if (Math.abs(delta[0]) < 0.01 && Math.abs(delta[1]) < 0.01) {
                            return;
                        }
                        drag = true;
                    }
                    return handler(id, 'mousemove', [x, y]);
                case 'mouseup':
                    if (drag === false) {
                        return handler(id, 'click', [x, y]);
                    }
                    return handler(id, 'mouseup', [x, y]);
                default:
                    break;
            }
        }
        const add_handlers = () => {
            if (!enabled) {
                this.canvas.addEventListener('mousemove', _mousemove);
                window.addEventListener('mouseup', _mouseup);
                enabled = true;
            }
        }
        const remove_handlers = () => {
            if (enabled) {
                this.canvas.removeEventListener('mousemove', _mousemove);
                window.removeEventListener('mouseup', _mouseup);
                enabled = false;
            }
        }
        const _mousemove = (e) => {
            e.preventDefault();
            _handler(e);
        }
        const _mouseup = (e) => {
            e.preventDefault();
            _handler(e);
            remove_handlers();
        }
        const _mousedown = (e) => {
            e.preventDefault();
            _handler(e);
           add_handlers();
        }
        this.canvas.addEventListener('mousedown', _mousedown);
        return {
            add_handlers,
            remove_handlers,
        }
    }
    bind_cursor_handler(handler) {
        let enabled = false;
        const _cursor_mousemove = (e) => {
            const mode = handler(this._mouse_to_relative(e));
            this._update_pointer(mode);
        }
        const add_handler = () => {
            if (!enabled) {
                this.canvas.addEventListener('mousemove', _cursor_mousemove);
                enabled = true;
            }
        }
        const remove_handler = () => {
            if (enabled) {
                this.canvas.removeEventListener('mousemove', _cursor_mousemove);
                enabled = false;
            }
        }
        return {
            add_handler,
            remove_handler,
        }
    }
}


class AnnotationController {
    constructor(c, annotations = []) {
        this.c = c;
        this._annotations = {}
        this.av = new DrawView(c);
        this.ev = new DrawView(c);

        // Mouse handler
        const {
            add_handlers,
            remove_handlers
        } = this.ev.bind_click_handler('ev', this._handle_events.bind(this));
        this._add_ev_handlers = add_handlers;
        this._remove_ev_handlers = remove_handlers;

        // Cursor handler
        const {
            add_handler: _add_cursor_handler,
            remove_handler: _remove_cursor_handler
        } = this.ev.bind_cursor_handler(this._handle_cursor.bind(this));
        this._add_cursor_handler = _add_cursor_handler;
        this._remove_cursor_handler = _remove_cursor_handler;

        // Editing state
        this._edit = [];
        this._offset = [-1, -1];
        this._delta = [0, 0];
        this._resizing = null;
        this._disabled = false;
        this.mode = 'idle';

        // Initialise annotations
        this.annotations = annotations;
        this.av.hidden = false;

        this.ev.hidden = false;
    }
    get disabled() {
        return this._disabled;
    }
    set disabled(val) {
        this._disabled = !!val;
        if (this._disabled) {
            this.av._update_pointer('disabled');
            this.ev.hidden = true;
        } else {
            this.av._update_pointer('default');
            this.ev.hidden = false;
        }
    }
    get width() {
        return this.av.width;
    }
    get height() {
        return this.av.height;
    }
    get edit() {
        return this._edit;
    }

    set edit(_val) {
        if (!(Array.isArray(_val))) {
            throw new Error(`Trying to set a non array value to edit id parameter - ${_val}`);
        }
        this._edit = _val;
        if (this._edit.length === 0) {
            this._remove_cursor_handler();
        } else {
            this._add_cursor_handler();
        }
    }

    get mode() {
        return this._mode;
    }

    set mode(_val) {
        switch(_val) {
            case 'idle':
                // Disable edit handlers
                this._remove_ev_handlers();
                break;
            case 'draw_or_select':
            case 'draw':
            case 'resize':
            case 'move':
                this._add_ev_handlers();
                break;
            default:
                throw new Error(`Unknown mode - ${_val}`);
        }
        this._mode = _val;
    }

    _get_point_position_in_annotation([x, y], id){
        const params = {
            cp: false,
            absolute: true,
        }
        if (this._edit.includes(id)) {
            params.cp = true;
        }
        const path = this.av.get_path(this._annotations[id], params);
        return this.av.is_point_in_path([x, y], path, true);
    }

    _calculateResizeDirection(where, id, [x, y]) {

        const [_x, _y, _w, _h] = this._annotations[id];

        const cx = _x + _w / 2;
        const cy = _y + _h / 2;

        const ys = `${ y > cy ? 'b' : 't'}`;
        const xs = `${ x > cx ? 'r' : 'l'}`;
        if (where === 'cp')
            return `${ys}${xs}`;

        // on the segment
        const mdy = Math.min(
            Math.abs(y - _y),
            Math.abs(y - _y - _h),
        );

        const mdx = Math.min(
            Math.abs(x - _x),
            Math.abs(x - _x - _w),
        )
        if (mdy < mdx) {
            // close to top or bottom
            return `${ys}${mdx < 0.01 ? xs : ''}`;
        } else {
            return `${mdy < 0.01 ? ys : ''}${xs}`;
        }
    }

    _handle_cursor([x, y]) {
        let pos = 'out';
        if (this._edit.length === 0) {
            return 'default';
        }

        const _id = this._edit.find((_id) => this._get_point_position_in_annotation([x, y], _id) !== 'out');
        if (_id) {
            pos = this._get_point_position_in_annotation([x, y], _id);
        }
        if (pos === 'cp' || pos === 'on') {
            return this._calculateResizeDirection(pos, _id, [x, y]);
        }
        return (pos === 'in' ? 'move' : 'default');
    }

    _handle_events(id, type, [x, y]) {

        console.log(id, type, [x, y], this.mode);
        switch(type){
            case 'click': {
                const _ids = [...this.edit, ...Object.keys(this._annotations)];
                let pos = 'out';
                const _id = _ids.find((_id) => this._get_point_position_in_annotation([x, y], _id) !== 'out');
                if (_id) {
                    pos = this._get_point_position_in_annotation([x, y], _id);
                }

                if (pos === 'out') {
                    // Deselect everything
                    this.edit = [];
                    this.draw();
                    this.mode = 'idle';
                    return;
                }

                // Click landed on an annotation - select it
                this.edit = [_id];
                this.draw();
                this.mode = 'idle';
                break;
            }
            case 'mousedown': {
                this._offset = [x, y];
                this._delta = [0, 0];

                // Look through the edit list first.
                // TODO make it set operations
                const _ids = [...this.edit, ...Object.keys(this._annotations)];
                let pos = 'out';
                let is_edited = false;
                const _id = _ids.find((_id) => this._get_point_position_in_annotation([x, y], _id) !== 'out');
                if (_id) {
                    pos = this._get_point_position_in_annotation([x, y], _id);
                    is_edited = this._edit.includes(_id);
                }
                if (pos === 'out') {
                    // Draw or deselect possible
                    this.mode = 'draw_or_select';
                } else if (pos === 'in') {
                    if (is_edited) {
                        this.mode = 'move';
                    } else {
                        // Draw or select possible
                        this.mode = 'draw_or_select';
                    }
                } else {
                    if (is_edited) {
                        this.mode = 'resize';
                        const _dir = this._calculateResizeDirection(pos, _id, [x, y]);
                        this._resizing = [_id, _dir];
                    } else {
                        this.mode = 'draw_or_select';
                    }
                }
                break;
            }
            case 'mousemove':
                this._delta = [x - this._offset[0], y - this._offset[1]];
                if (this.mode === 'draw_or_select') {
                    // Select didn't happen, so should be drawing
                    this.edit = [];
                    this.draw();
                    this.mode = 'draw';
                }
                this._draw_edit();
                break;
            case 'mouseup':
                // Finalize based on mode
                const _coords = this._get_transformed_coords();
                if (this.mode === 'draw') {
                    const _add_id = this._add(_coords[0]);
                    this.edit = [_add_id];
                    const detail = [{
                        type: 'add',
                        id: _add_id,
                        bbox: _coords[0],
                    }];
                    this.c.dispatchEvent(new CustomEvent('annotation', {
                        detail
                    }));
                } else if (this.mode === 'move') {
                    this._edit.forEach((_id, idx) => {
                        this._annotations[_id] = _coords[idx];
                    });
                    const detail = this._edit.map((_id, idx) => {
                        return {
                            type: 'update',
                            id: _id,
                            bbox: _coords[idx],
                        }
                    })
                    this.c.dispatchEvent(new CustomEvent('annotation', {
                        detail
                    }));
                } else if (this.mode === 'resize') {
                    const [_id, _dir] = this._resizing
                    this._annotations[_id] = _coords[0];
                    const detail = [
                        {
                            type: 'update',
                            id: _id,
                            bbox: _coords[0],
                        }
                    ];
                    this.c.dispatchEvent(new CustomEvent('annotation', {
                        detail
                    }));
                } else {
                    console.error(`Didnt expect mode ${this.mode} in mouseup`);
                }

                this.mode = 'idle';
                this._offset = [-1, -1];
                this._delta = [0, 0];
                this._resizing = null;
                this._draw_edit();
                break;
            default:
                break;
        }
    }

    on_change() {
        this.av.on_change();
        this.ev.on_change();
        this.draw();
    }

    _get_transformed_coords() {
        // Based on offset, delta and mode
        // draw, move, resize temporary coordinates and finalize
        // on mouseup
        const { _offset: [ox, oy], _delta: [dx, dy]} = this;

        switch(this.mode) {
            case 'draw':
                return [[ox, oy, dx, dy]];
            case 'move':
                return this._edit.map((_id) => {
                    const [x, y, w, h] = this._annotations[_id]
                    return [x + dx, y + dy, w, h];
                });
            case 'resize':
                const [_id, _dir] = this._resizing;
                const [_x, _y, _w, _h] = this._annotations[_id];

                const t = _dir.startsWith('t');
                const b = _dir.startsWith('b');
                const l = _dir.endsWith('l');
                const r = _dir.endsWith('r');
                const _resized = [
                    l ? _x + dx : _x,
                    t ? _y + dy : _y,
                    r ? _w + dx : (l ? _w - dx : _w),
                    b ? _h + dy : (t ? _h - dy : _h),
                ];
                if (_resized[2] < 0 || _resized[3] < 0) {
                    _resized[0] += (_resized[2] < 0 ? _resized[2] : 0);
                    _resized[2] = Math.abs(_resized[2]);
                    _resized[1] += (_resized[3] < 0 ? _resized[3] : 0);
                    _resized[3] = Math.abs(_resized[3]);
                }
                return [_resized];
            default:
                return this._edit.map(_id => this._annotations[_id]);
        }

    }

    _draw_edit(show=true) {
        this.ev.draw(
            this._get_transformed_coords(),
            {
                clear: true,
                fill: true,
                cp: (this.mode === 'draw' ? false : true),
                absolute: true,
                show,
            }
        );
    }

    _draw_annotations(show=true) {
        let _keys = Object.keys(this._annotations);
        // TODO: Change to set operations
        if (this._edit.length > 0) {
            _keys = _keys.filter(x => !(this._edit.includes(x)))
        }
        this.av.draw(
            _keys.map(_id => this._annotations[_id]),
            {
                clear: true,
                fill: false,
                cp: false,
                absolute: true,
                show,
            }
        );
    }

    draw() {
        this._draw_edit(true);
        this._draw_annotations(true);
    }

    get annotations() {
        return Object.values(this._annotations);
    }

    set annotations(val) {
        this._annotations = {}
        this.edit = [];
        this.add(val);
    }

    _add(annotation) {
        const _id = create_random_id();
        this._annotations[_id] = annotation;
        return _id;
    }

    add(annotations) {
        annotations.forEach((_annotation) => {
            this._add(_annotation);
        });
        this.draw();
    }
}

class SingleAnnotationController extends AnnotationController {
    constructor(c) {
        super(c, []);
    }
    get mode() {
        return super.mode;
    }
    set mode(_val) {
        if(this._mode === _val) {
            console.error(`Ignoring state transition ${this._mode} -> ${_val}`);
            return;
        }

        if (_val === 'draw') {
            // Clear annotations dict
            this.annotations = [];
        }

        super.mode = _val;
    }
}