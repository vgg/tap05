const MAX_DIM = 1024;

const resize_canvas = document.createElement("canvas");
const resize_ctx = resize_canvas.getContext("2d", { alpha: false});

// Geometric
const tcanvas = document.createElement('canvas');
const render = setup_gl(tcanvas);

// Photometric
const pcanvas = document.createElement('canvas');
const prender = setup_gl(pcanvas, 'photometric');

// Diff
const dcanvas = document.createElement('canvas');
const dctx = dcanvas.getContext('2d');

// Temporary canvas
const bcanvas = document.createElement('canvas');
const bctx = bcanvas.getContext('2d');

// Download element
const download_el = document.createElement('a');

const getResizeDimensions = ([iw, ih], _dim) => {
    if (iw <= _dim && ih <= _dim) {
        return [iw, ih];
    }
    const max_dim = iw > ih ? iw : ih;
    const ow = (iw / max_dim) * _dim,
      oh = (ih / max_dim) * _dim;
    return [ow, oh];
}

const cropAndResizeImage = (img, ctx, roi, [ow, oh]) => {
    const c = ctx.canvas;
    ctx.clearRect(0, 0, c.width, c.height);

    c.width = Math.round(ow);
    c.height = Math.round(oh);
    ctx.drawImage(img, roi.x, roi.y, roi.width, roi.height, 0, 0, c.width, c.height);
}
const getImageData = (img, ctx, roi, [ow, oh]) => {
    cropAndResizeImage(img, ctx, roi, [ow, oh]);
    return ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
};

const mat_vec_multiply = (H, pt) => {
    // Apply H, divide by z term
    // H is length 9 (3 x 3)
    // pt is length 2
    const [x, y] = pt
    const new_pt = [
        (H[0]*x + H[1]*y + H[2]),
        (H[3]*x + H[4]*y + H[5]),
        (H[6]*x + H[7]*y + H[8]),
    ]
    const x_dash = new_pt[0] / new_pt[2];
    const y_dash = new_pt[1] / new_pt[2];
    return [x_dash, y_dash];
}
const get_corners_for_bbox = (bbox) => {
    return [
        [bbox.x, bbox.y],
        [bbox.x + bbox.width, bbox.y],
        [bbox.x + bbox.width, bbox.y + bbox.height],
        [bbox.x, bbox.y + bbox.height]
    ];
}
const get_bbox_from_corners = (corners) => {
    const [minx, miny, maxx, maxy] = corners.reduce(([mx, my, Mx, My], [x, y]) => {
        if (mx > x) {
            mx = x;
        }
        if (my > y) {
            my = y;
        }
        if (Mx < x) {
            Mx = x;
        }
        if (My < y) {
            My = y;
        }
        return [mx, my, Mx, My];
    }, [Infinity, Infinity, -Infinity, -Infinity]);

    return {
        x: minx,
        y: miny,
        width: (maxx - minx),
        height: (maxy - miny),
    }
}
const pad_bbox = (bbox, _pad) => {
    let pad = _pad;
    if (pad > 1.0) {
        pad = pad / 100;
    }
    bbox.width *= (1 + pad);
    bbox.height *= (1 + pad);
    bbox.x -= (pad/2)*bbox.width;
    bbox.y -= (pad/2)*bbox.height;

    return bbox;
}
const invert_bbox = (bbox, H) => {
    // Get 4 corners -> Homogeneous coordinates
    const four_corners = get_corners_for_bbox(bbox);

    let _H = H;
    if (H.length !== 9) {
        const n_cp = H.length / 4 - 3;
        const offset = 4*n_cp
        _H = new Float32Array([
            H[offset+4],
            H[offset+8],
            H[offset],
            H[offset+5],
            H[offset+9],
            H[offset+1],
            0,
            0,
            1.0
        ]);
    }

    // multiply
    const four_corners_inv = four_corners.map((pt) => mat_vec_multiply(_H, pt));
    let bbox_inv = get_bbox_from_corners(four_corners_inv);

    // return
    return bbox_inv
}
const clamp_bbox_to_image_dims = (bbox, [iw, ih]) => {
    bbox.x = (bbox.x < 0 ? 0 : bbox.x);
    bbox.y = (bbox.y < 0 ? 0 : bbox.y);
    bbox.width = (
        bbox.x + bbox.width > iw
        ? (iw - bbox.x)
        : bbox.width
    )
    bbox.height = (
        bbox.y + bbox.height > ih
        ? (ih - bbox.y)
        : bbox.height
    )
    return bbox
}
const getTransform = (
        target,
        query,
        bbox,
        transform,
        _last_H = null) => {

    const query_iw = query.naturalWidth || query.width,
        query_ih = query.naturalHeight || query.height;
    let bbox_query = {
        x: 0,
        y: 0,
        width: query_iw,
        height: query_ih,
    }
    let padded_bbox_query = Object.assign({}, bbox_query);

    if (_last_H !== null) {
        // Invert bbox and take
        bbox_query = invert_bbox(bbox, _last_H);
        padded_bbox_query = Object.assign({}, bbox_query);

        //Pin it within the image
        bbox_query = clamp_bbox_to_image_dims(bbox_query, [query_iw, query_ih]);

        // pad 10%
        padded_bbox_query = clamp_bbox_to_image_dims(pad_bbox(padded_bbox_query, 10), [query_iw, query_ih]);
    }


    const [target_ow, target_oh] = getResizeDimensions([bbox.width, bbox.height], MAX_DIM);
    const [query_ow, query_oh] = getResizeDimensions([padded_bbox_query.width, padded_bbox_query.height], MAX_DIM);

    const target_data = getImageData(target, resize_ctx, bbox, [target_ow, target_oh]);
    const query_data = getImageData(query, resize_ctx, padded_bbox_query, [query_ow, query_oh]);

    const buf = Module.estimate_transform(
        query_data.data,
        query_data.height,
        target_data.data,
        target_data.height,
        transform,
        (query_ow / padded_bbox_query.width),
        (target_ow / bbox.width),
    );

    const H = new Float64Array(
      buf.buffer,
      buf.byteOffset,
      buf.length / Float64Array.BYTES_PER_ELEMENT
    ).slice();

    const ret = {
        H,
        bbox_query,
        padded_bbox_query
    }

    if (transform !== 'tps') {
        const _inv_buf = Module.invert_transform(H);
        const H_inv = new Float64Array(
            _inv_buf.buffer,
            _inv_buf.byteOffset,
            _inv_buf.length / Float64Array.BYTES_PER_ELEMENT
        ).slice();
        ret['H_inv'] = H_inv;
    }

    const source_meanstddev = Module.estimate_luma_mean_stddev(
        query_data.data,
        query_data.height,
    );
    const target_meanstddev = Module.estimate_luma_mean_stddev(
        target_data.data,
        target_data.height
    );
    ret['source_meanstddev'] = source_meanstddev.slice();
    ret['target_meanstddev'] = target_meanstddev.slice();

    return ret;
}

const visualisations = (im1, im2, ctx, bbox) => {
    const MIN_ZOOM = 1.0;
    const MAX_ZOOM = 16.0;
    const get_image_bounds = (img, cw, ch) => {
        const iw = img.naturalWidth || img.width;
        const ih = img.naturalHeight || img.height;

        let rh = 0;
        let rw = 0;
        // Get Image and Rendered Canvas Aspect ratio
        const iar = iw / ih;
        const ar = cw / ch;

        if (iar < ar) {
            // Rendered on a wider canvas
            rh = ch;
            rw = rh * iar;
        } else {
            // Rendered on a taller canvas
            rw = cw;
            rh = rw / iar;
        }
        return {
            offset_x: (cw - rw) / 2,
            offset_y: (ch - rh) / 2,
            width: rw,
            height: rh
        }
    }
    const channels = {
        red: "#F00",
        green: "#0F0",
        blue: "#00F",
        bg: "#0FF",
        rg: "#FF0",
        rb: "#F0F",
        rgb: "#FFF",
    };

    let screen_dim = {
        width: 0,
        height: 0,
        offset_x: 0,
        offset_y: 0,
        rwidth: 0,
        rheight: 0,
    };

    let current_zoom = {
        scale: 1.0,
        tx: 0,
        ty: 0,
    }

    let zoom_enabled = false;
    let _mousemove_enabled = false;
    let _click_enabled = false;
    let _click_locked = false;
    const debounce = (handler, t = 250) => {
        let timeout = null;
        return () => {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(handler, t);
        }
    };

    const on_resize = () => {
        const { width, height } = ctx.canvas.getBoundingClientRect();
        const { offset_x, offset_y, width: rwidth, height: rheight} = get_image_bounds(ctx.canvas, width, height);
        screen_dim = {
            width,
            height,
            offset_x,
            offset_y,
            rwidth,
            rheight,
        }
    }

    const debounce_resize = debounce(on_resize);

    const _add_resize_handler = () => {
        window.addEventListener('resize', debounce_resize);
        on_resize();
    }
    const _remove_resize_handler = () => {
        window.removeEventListener('resize', debounce_resize);
    }
    const _mousemove = (e) => {
        e.preventDefault();

        set_zoom({
            ...current_zoom,
            tx: e.offsetX,
            ty: e.offsetY,
        });
    }

    const set_scale = (_scale) => {
        set_zoom({ scale: _scale});
    }
    const get_scale = () => {
        return current_zoom.scale;
    }
    const _click = (e) => {
        e.stopPropagation();
        e.preventDefault();

        let tx = e.offsetX;
        let ty = e.offsetY;


        set_zoom({
            tx,
            ty
        });
        _click_locked = !_click_locked;
        if (_click_locked) {
            // remove mouse move
            _remove_mousemove_handler();
        } else {
            // add mouse move
            _add_mousemove_handler();
        }
    }
    const _mousewheel = (e) => {
        e.stopPropagation();
        e.preventDefault();

        let tx = (_click_locked ? current_zoom.tx : e.offsetX);
        let ty = (_click_locked ? current_zoom.ty : e.offsetY);

        set_zoom({
            scale: current_zoom.scale - (e.deltaY > 0 ? 1 : -1),
            tx,
            ty,
        })

    }
    const _add_mousemove_handler = () => {
        if (_mousemove_enabled || _click_locked) {
            return;
        }
        ctx.canvas.addEventListener('mousemove', _mousemove);
        _mousemove_enabled = true;

    }
    const _add_click_handler = () => {
        if (_click_enabled) {
            return;
        }
        ctx.canvas.addEventListener('click', _click);
        _click_enabled = true;
    }
    const _add_mouse_handler = () => {
        _add_click_handler();
        _add_mousemove_handler();
    }
    const _remove_mousemove_handler = () => {
        if (!_mousemove_enabled) {
            return;
        }
        ctx.canvas.removeEventListener('mousemove', _mousemove);
        _mousemove_enabled = false;
    }
    const _remove_click_handler = () => {
        if (!_click_enabled) {
            return;
        }
        ctx.canvas.removeEventListener('click', _click);
        _click_enabled = false;
        _click_locked = false;
    }
    const _remove_mouse_handler = () => {
        _remove_click_handler();
        _remove_mousemove_handler();
    }
    const _remove_wheel_handler = () => {
        ctx.canvas.removeEventListener('wheel', _mousewheel);
    }
    const _add_wheel_handler = () => {
        ctx.canvas.addEventListener('wheel', _mousewheel);
    }

    const disable_zoom = (reset = true) => {
        if (!zoom_enabled) {
            return;
        }
        _remove_resize_handler();
        _remove_wheel_handler();
        zoom_enabled = false;
        if (reset) {
            set_zoom({
                scale: 1,
                tx: 0,
                ty: 0,
            })
        } else {
            redraw();
        }
    }
    const enable_zoom = () => {
        if (zoom_enabled) {
            return;
        }
        _add_resize_handler();
        _add_wheel_handler();
        zoom_enabled = true;
        redraw();
    }
    let _updating = false;
    const set_zoom = (_zoom) => {
        const _scale =  Math.max(1.0, Math.min(8.0, _zoom.scale || current_zoom.scale));
        if (_scale !== current_zoom.scale) {
            ctx.canvas.dispatchEvent(new CustomEvent('zoom', {
                detail: {
                    scale: _scale
                }
            }));
        }
        current_zoom = {
            ...current_zoom,
            ..._zoom,
            scale: _scale,
        }
        if (current_zoom.scale === 1.0) {
            _remove_mouse_handler();
        } else {
            _add_mouse_handler();
        }
        redraw();
    }
    const redraw = () => {
        if (_updating) {
            return;
        }
        _updating = true;
        window.requestAnimationFrame(() => {
            _updating = false;
            draw();
        });
    }

    const draw = (_zoom_options) => {
        const { scale, tx, ty } = _zoom_options || current_zoom;
        const { width, height } = ctx.canvas;
        const { offset_x, offset_y, rwidth: rw, rheight: rh } = screen_dim;
        copyImage(ctx, bctx.canvas);
        if (scale === 1.0 || !zoom_enabled) {
            return;
        }

        // Zoom (Mouse coordinates to canvas coordinates)
        let ix = Math.round(Math.min(rw, Math.max(0, tx - offset_x)));
        let iy = Math.round(Math.min(rh, Math.max(0, ty - offset_y)));
        ix *= (width / rw);
        iy *= (height / rh);
        // Zoom radius based on scale
        const zoom_radius = Math.round(0.30 * Math.min(width, height));
        const scaled_dim = Math.round(zoom_radius / scale);

        // Draw black overlay layer
        ctx.save();
        ctx.fillStyle = 'rgba(0,0,0,0.65)';
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.restore();

        // Draw clipping circle
        ctx.save();
        ctx.beginPath();
        ctx.arc(ix, iy, zoom_radius, 0, Math.PI * 2, true);
        ctx.clip();

        // Draw bcanvas into clipping circle
        ctx.globalCompositeOperation = 'copy';
        ctx.globalAlpha = 1.0;

        ctx.drawImage(
            bctx.canvas,
            ix - scaled_dim,
            iy - scaled_dim,
            scaled_dim *2,
            scaled_dim *2,
            ix - zoom_radius,
            iy - zoom_radius,
            zoom_radius * 2,
            zoom_radius * 2
        );
        ctx.globalCompositeOperation = 'source-over';

        ctx.beginPath();
        ctx.arc(ix, iy, zoom_radius - 1, 0, Math.PI * 2, true);
        ctx.strokeStyle = _click_locked ? 'blue' : 'yellow';
        ctx.lineWidth = '2';
        ctx.stroke();
        ctx.restore();
    }

    const copyImage = (_ctx, _img, bbox = null) => {
        // Copy image to canvas
        const iw = _img.naturalWidth || _img.width;
        const ih = _img.naturalHeight || _img.height;
        _ctx.canvas.width = (bbox === null ? iw : bbox.width);
        _ctx.canvas.height = (bbox === null ? ih : bbox.height);
        _ctx.clearRect(0, 0, _ctx.canvas.width, _ctx.canvas.height);
        if (bbox === null) {
            _ctx.drawImage(_img, 0, 0);
        } else {
            _ctx.drawImage(_img,
                bbox.x,
                bbox.y,
                bbox.width,
                bbox.height,
                0,
                0,
                _ctx.canvas.width,
                _ctx.canvas.height
            );
        }
    };

    const to_blob = (canvas) => {
        return new Promise((resolve) => {
            canvas.toBlob((blob) => {
                resolve(blob);
            }, 'image/png');
        });
    }

    const save_gif = async (delay) => {
        const w = im2.naturalWidth || im2.width;
        const h = im2.naturalHeight || im2.height;
        // Copy image to canvas
        copyImage(bctx, im1, bbox);

        // export to blob
        const im1_blob = await to_blob(bcanvas);
        const im1_url= URL.createObjectURL(im1_blob);

        // Copy image to canvas
        copyImage(bctx, im2);

        // export to blob
        const im2_blob = await to_blob(bcanvas);
        const im2_url= URL.createObjectURL(im2_blob);

        // Make gif
        const fname = 'toggle.gif';

        gifshot.createGIF({
            images: [im1_url, im2_url],
            gifWidth: w,
            gifHeight: h,
            frameDuration: Math.round(delay / 100),
        }, (result) => {
            if (!result.error) {
                download_el.href = result.image;
                download_el.download = fname;
                download_el.click();
            } else {
                console.error("Exporting to GIF failed!");
            }
            URL.revokeObjectURL(im1_url);
            URL.revokeObjectURL(im2_url);
        });
    }

    const save_result = async (mode) => {
        const _blob = await to_blob(ctx.canvas);
        const _url = URL.createObjectURL(_blob);
        download_el.href = _url;
        download_el.download = `${mode}.png`;
        download_el.click();
    }

    const multiplyChannel = (_ctx, cname) => {
        _ctx.save();
        _ctx.fillStyle = channels[cname] || "#000";
        _ctx.globalCompositeOperation = "multiply";
        _ctx.fillRect(0, 0, _ctx.canvas.width, _ctx.canvas.height);
        _ctx.restore();
    };

    const grayscale = (_ctx) => {
        _ctx.save();
        _ctx.fillStyle = "#000";
        _ctx.globalCompositeOperation = "saturation";
        _ctx.fillRect(0, 0, _ctx.canvas.width, _ctx.canvas.height);
        _ctx.restore();
    };

    const draw_toggle_image = (flag) => {
        if (bctx.canvas.width !== ctx.canvas.width || bctx.canvas.height !== ctx.canvas.height) {
            bctx.canvas.width = ctx.canvas.width;
            bctx.canvas.height = ctx.canvas.height;
        }

        if (flag === false) {
            bctx.clearRect(0, 0, bctx.canvas.width, bctx.canvas.height);
        }

        if (flag) {
            bctx.drawImage(
                im2,
                0,
                0
            );
        } else {
            bctx.drawImage(
                im1,
                bbox.x,
                bbox.y,
                bbox.width,
                bbox.height,
                0,
                0,
                ctx.canvas.width,
                ctx.canvas.height,
            )
        }
        draw();

    };

    let toggle_flag = false;
    let toggle_timer = null;
    const get_toggle_cb = (delay) => {
        const cb = () => {
            toggle_timer = setTimeout(cb, delay);
            draw_toggle_image(toggle_flag);
            toggle_flag = !toggle_flag;
        };
        return cb;
    }

    const start_toggle = (delay) => {
        stop_toggle();
        if (!delay || delay < 100) {
            return;
        }
        toggle_timer = setTimeout(get_toggle_cb(delay), 0);
    }

    const stop_toggle = () => {
        if (toggle_timer) {
            clearTimeout(toggle_timer);
            toggle_timer = null;
        }
    }

    const draw_diff_image = () => {
        stop_toggle();
        // Based on
        // 1. https://stackoverflow.com/questions/60937639/canvas-splitting-image-into-rgba-components
        // 2. https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Compositing
        // 3. https://stackoverflow.com/questions/33822092/greyscale-canvas-make-canvas-color-to-black-and-white
        // Copy image
        copyImage(dctx, im1, bbox);

        // gray scale
        grayscale(dctx);

        // Get channel
        multiplyChannel(dctx, "red");

        // Copy it to diff canvas
        copyImage(bctx, dcanvas);

        // copy image2
        copyImage(dctx, im2);

        // grayscale
        grayscale(dctx);

        // multiply blue-green
        multiplyChannel(dctx, "bg");

        // Copy it over diff canvas
        bctx.save();
        bctx.globalCompositeOperation = "lighter";
        bctx.drawImage(dcanvas, 0, 0);
        bctx.restore();

        // Transfer to result
        draw();
    };

    const draw_overlay = (factor) => {
        stop_toggle();

        bctx.canvas.width = ctx.canvas.width;
        bctx.canvas.height = ctx.canvas.height;
        bctx.clearRect(0, 0, bctx.canvas.width, bctx.canvas.height);

        bctx.save();
        bctx.drawImage(im2, 0, 0);
        bctx.globalAlpha = factor;
        bctx.drawImage(im1,
            bbox.x,
            bbox.y,
            bbox.width,
            bbox.height,
            0,
            0,
            bctx.canvas.width,
            bctx.canvas.height,
        );
        bctx.restore();

        draw();
    }

    return {
        enable_zoom,
        disable_zoom,
        set_scale,
        get_scale,
        overlay: draw_overlay,
        toggle: start_toggle,
        diff: draw_diff_image,
        save_gif,
        save_result,
    };
}

const compare = (query, target, ctx, transform = "affine", _bbox = null, _H = null ) => {
    const target_iw = target.naturalWidth || target.width,
        target_ih = target.naturalHeight || target.height;

    let bbox = _bbox;
    let H_input = _H;
    let H_inv = null;


    if (_bbox === null) {
        // Box not provided
        // consider full image
        bbox = {
            x: 0,
            y: 0,
            width: target_iw,
            height: target_ih,
        }
    } else {
        // Box is provided
        bbox = {
            x: _bbox.x * target_iw,
            y: _bbox.y * target_ih,
            width: _bbox.width * target_iw,
            height: _bbox.height * target_ih,
        }
    }
    if( _H === null && (_bbox !== null || transform === "tps")) {
        // Case where a box is drawn before compare, or tps
        const _target_bbox = {
            x: 0,
            y: 0,
            width: target_iw,
            height: target_ih,
        }
        const _ret = getTransform(target, query, _target_bbox, "affine", null);
        H_input = _ret.H.slice();
        H_inv = _ret['H_inv'].slice();
    }

    const ret = getTransform(target, query, bbox, transform, H_input);
    const { H, bbox_query, padded_bbox_query } = ret;

    cropAndResizeImage(query, resize_ctx, padded_bbox_query, [padded_bbox_query.width, padded_bbox_query.height])
    ctx.canvas.width = bcanvas.width = tcanvas.width = bbox.width;
    ctx.canvas.height = bcanvas.height = tcanvas.height = bbox.height;

    render(resize_canvas, H);

    const { source_meanstddev, target_meanstddev } = ret;
    const disable_photometric = () => {
        render(resize_canvas, H, true);
    };
    const enable_photometric = () => {
        pcanvas.width = tcanvas.width;
        pcanvas.height = tcanvas.height;
        prender(tcanvas, source_meanstddev, target_meanstddev);
        render(pcanvas, null, true);
    }

    result = {
        H: (H_inv !== null ? H_input : H),
        ...(H_inv !== null && { H_inv: H_inv }),
        visualisations: visualisations(target, tcanvas, ctx, bbox),
        bbox_query,
        enable_photometric,
        disable_photometric,
    }

    if (_bbox === null && 'H_inv' in ret) {
        // Compute the target bbox
        const _H_inv = ret['H_inv'];
        const four_corners = get_corners_for_bbox(bbox_query);
        const target_corners = four_corners.map((pt) => mat_vec_multiply(_H_inv, pt));
        const target_bbox = get_bbox_from_corners(target_corners);
        result['bbox_target'] = target_bbox;
        result['H_inv'] = _H_inv;
    }
    return result;
}