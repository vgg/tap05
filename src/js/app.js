

class ImageContainer {
    /**
     * Accepts the container to manage w.r.t
     * to handling image inputs, drawing a bbox
     * and emitting events
     *
     * @param {HTMLElement} c
     * @param {HTMLInputElement} input_el
     */
    constructor(c, input_el) {
        this.c = c;
        this.btn_c = c.querySelector('div.icon-buttons');
        this.img_c = c.querySelector('div.wrapper');
        this.annotation_controller = null;
        this.input_el = input_el;

        // Listeners
        this.handle_input_change = this._handle_input_change.bind(this);

        this.btn_c.onclick = (e) => {
            if (e.target === e.currentTarget) {
                // Clicked on the containing div
                return;
            }
            const action = e.target.dataset.action;
            this.handle_btn_click(action);
        }

        this.img_c.onclick = () => {
            this.input_el.onchange = this.handle_input_change;
            this.input_el.click();
        }
    }
    disable_annotations() {
        this.annotation_controller.disabled = true;
    }
    enable_annotations() {
        this.annotation_controller.disabled = false;
    }
    reset_annotations() {
        this.annotation_controller.annotations = [];
    }

    on_resize() {
        if (this.annotation_controller) {
            this.annotation_controller.on_change();
        }
    }

    _handle_input_change(e) {
        if (e.target.files.length === 0) {
            return;
        }

        const _file = e.target.files[0];
        let img = this.img_c.querySelector("img");
        if (!img) {
            img = document.createElement("img");
            this.img_c.appendChild(img);
        }
        img.onload = () => {
            URL.revokeObjectURL(img.src);
            if (this.img_c.onclick) {
                this.btn_c.classList.remove('gone');
                this.img_c.onclick = null;
            }
            if(!this.annotation_controller) {
                this.annotation_controller = new SingleAnnotationController(this.img_c);
            }
            this.annotation_controller.on_change();
            this.c.dispatchEvent(new CustomEvent("image_change"));
        };
        img.onerror = () => {
            console.error(`Couldn't load image "${_file.name}"`);
            img.remove();
        };
        img.src = URL.createObjectURL(_file);
        e.target.value = "";
    };

    handle_btn_click(action) {
        switch (action) {
            case "add":
                this.input_el.onchange = this.handle_input_change;
                this.input_el.click();
                break;
            case "clear":
                break;
        }
    };

}